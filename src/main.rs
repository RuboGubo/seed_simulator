mod field;
mod cell;
mod cord;

use field::Field;
use cell::Cell;

fn main() {
    println!("yay");

    let mut field = Field::new();
    field.b_place(Cell::Seed, vec![Cell::Any], field.get_middle_cord().expect("The middle is apparently not 'Anything'"));
    println!("{}", field)
}
