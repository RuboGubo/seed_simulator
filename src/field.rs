use crate::cell::Cell;
use crate::cord::Cord;
use crate::cord::Cords;

#[derive(Debug, PartialEq)]
pub struct Field {
    field: Vec<Vec<Cell>>,
    hight: usize,
    width: usize,
}

impl Field {
    pub fn new() -> Self {
        let width = 10;
        let hight = 10;
        let field = vec![vec![Cell::Soil; width]; hight];

        let field = Self { field, hight, width };
        field
    }
}

impl Field {
    // Placing Functions
    pub fn b_place(&mut self, item: Cell, items: Vec<Cell>, cords: Cords) -> &mut Self{
        for cord in cords {
            self.place_cord(&item, &items, cord)
        }
        self
    }

    fn get_cord<'a>(&'a self, cord: &'a Cord) -> Result<&'a Cell, String> {
        let error_msg: String = "cord does not exist".to_owned();
        self.field
            .get(cord.x).ok_or(error_msg.clone())?
            .get(cord.y).ok_or(error_msg)
    }

    fn place_cord(&mut self, item:&Cell, items: &Vec<Cell>, cord:Cord) {
        let cell = self.get_cord(&cord).unwrap();
        if items.contains(cell) || *cell == Cell::Any {
            self.field
                .get_mut(cord.x).unwrap()[cord.y] = item.clone();
        }
    }

    pub fn get_middle_cord(&self) -> Result<Cords, String> {
        let x_middle = self.width / 2;
        let y_middle = self.hight / 2;
        Ok(vec![Cord::new(x_middle, y_middle)])
    }
}

impl std::fmt::Display for Field {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let display_field: String = self
            .field
            .iter()
            .map(|row| {
                row.iter()
                    .map(|cell| match cell {
                        Cell::Soil => '.',
                        Cell::Rock => 'X',
                        Cell::Seed => 'S',
                        Cell::Plant => 'P',
                        Cell::Any => '*',
                    })
                    .collect::<String>()
                    + "\n"
            })
            .collect();
        write!(f, "{}", display_field)
    }
}
