#[derive(Debug, Clone, PartialEq)]
pub enum Cell {
    Soil,
    Rock,
    Seed,
    Plant,
    Any,
}