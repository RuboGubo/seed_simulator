pub type Cords = Vec<Cord>;

pub struct Cord {
    pub x: usize,
    pub y: usize,
}

impl Cord {
    pub fn new(x: usize, y:usize) -> Self {
        Self { x, y }
    }
}